define(['angularRoute','angularUIRouter'],function(){
    var app=angular.module('yourapp',['ngRoute','ui.router']);
    app.config(['$stateProvider','$urlRouterProvider',function($stateProvider, $urlRouterProvider){

        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('login', {
                url: '/',
                views: {
                    'content': {
                        templateUrl: 'views/login.html',
                        controller: "loginController"
                    }
                }
            })

    }]);
    return app;
});