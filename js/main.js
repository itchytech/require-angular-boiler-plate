require.config({
    paths: {
        'jquery': 'external/jquery/jquery',
        'angular': 'external/angular/angular',
        'angularRoute':'external/angular/angular-route',
        'angularUIRouter':'external/angular/angular-ui-router',
        'mainCtrl': 'app/controllers/mainController',
        'loginCtrl':'app/controllers/loginController',
        'app':'app'
    },
    shim: {
        'angular':{
            deps: ['jquery'],
            exports: 'angular'
        },
        angularRoute:['angular'],
        angularUIRouter:['angular'],
        'app': ['angular','angularRoute','angularUIRouter'],
        'mainCtrl':['angular','app'],
        'loginCtrl':['app']
    },
    baseURL:'js'
});


require(['angular','app','mainCtrl','loginCtrl'],function(angular){
    angular.bootstrap(angular.element('html'),['yourapp']);
});
