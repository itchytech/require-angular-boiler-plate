({
    baseUrl: '../js',
    mainConfigFile: '../js/main.js',
    name: '../js/main',
    out: '../js/concatenated-modules-min-no-license.js',
    preserveLicenseComments: false
})