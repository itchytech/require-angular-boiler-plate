({
    baseUrl: '../js',
    mainConfigFile: '../js/main.js',
    name: '../js/main',
    out: '../js/yourapp.min.js',
    preserveLicenseComments: false,
    paths:{
        requireLib:'../js/external/require/require'
    },
    include:'requireLib'
})